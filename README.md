# Terminalfour SiteManager v8 Docker Image (unofficial)

Install `docker` and `docker-compose` and get a full environment to support `Terminalfour SiteManager v7.4/v8.2/v8.3` running very quickly.

## With Docker

Use this image as the base for your custom app server image provide custom `server.xml` along with anything else.

### Tags

`7.4` v7.4 Tomcat 7, Oracle Java 8

`8.2` v8.2 Tomcat 7, Oracle Java 7

`8.3` v8.3 Tomcat 9, OpenJDK 11

### Example Dockerfile

```
FROM theadrum/t4-sitemanager:8.2
COPY server.xml /opt/apache-tomcat/conf/server.xml
EXPOSE 8080
```

Environment `CATALINA_OPTS` can be overridden in your `Dockerfile` so you can pass params to Tomcat for memory settings and others.

Build and Run in your prefered way with volumes and host names to support your licence

Use volumes for `/usr/terminalfour/contentstore`  `/usr/terminalfour/www` and /opt/apache-tomcat/webapps/terminalfour.war

## With Docker Compose

Best and fastest way to get a complete full running system is to use `docker-compose` from this repo. (Change hostname to suite licence)

Currently Tomcat, Oracle Java, MySQL and Apache Httpd server all running as seperate containers. I intent to continue to simplfy the setup of this image so that hostnames can be provided as environment vars at runtime.

1) Clone this repo from bitbucket and perform the following steps in the sub folder corresponding to the version number of SiteManager

2) Create a folder for the website called `www` in the root

3) Get the latest .war from Terminalfour and call it `terminalfour-v{verison}.war` in the root

4) Bring up the containers with `sudo docker-compose up`

5) Once build is complete visit the backend at `http://localhost:8080/terminalfour`

6) Install using contentstore path of `/usr/terminalfour/contentstore`

7) Once install is complete create publish channel at `/usr/terminalfour/www`  

8) Visit http://localhost:80/ to see published site.
